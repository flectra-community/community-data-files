# Flectra Community / community-data-files

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[account_tax_unece](account_tax_unece/) | 2.0.1.0.0| UNECE nomenclature for taxes
[base_unece](base_unece/) | 2.0.1.1.0| Base module for UNECE code lists
[account_payment_unece](account_payment_unece/) | 2.0.1.0.0| UNECE nomenclature for the payment methods
[base_iso3166](base_iso3166/) | 2.0.1.0.1| ISO 3166
[uom_unece](uom_unece/) | 2.0.1.0.0| UNECE nomenclature for the units of measure
[l10n_eu_nace](l10n_eu_nace/) | 2.0.1.0.0| European NACE partner categories
[base_currency_iso_4217](base_currency_iso_4217/) | 2.0.1.0.0| Adds numeric code and full name to currencies, following the ISO 4217 specification
[base_bank_from_iban](base_bank_from_iban/) | 2.0.1.0.1| Bank from IBAN


